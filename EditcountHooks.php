<?php

use MediaWiki\MediaWikiServices;

class EditcountHooks {

	/**
	 * @param Parser $parser
	 * @throws MWException
	 */
	public static function onParserFirstCallInit( Parser $parser ) {
		$parser->setFunctionHook( 'editcount', [ self::class, 'renderEditcount' ] );
	}

	/**
	 * Renders the editcount parser function
	 *
	 * @param Parser $parser
	 * @param string $username
	 * @param string|null $namespace
	 * @return string
	 */
	public static function renderEditcount( Parser $parser, string $username, string $namespace = null ) {
		$services = MediaWikiServices::getInstance();
		$user = $services->getUserFactory()->newFromName( $username );

		$uid = ( $user instanceof User ? $user->getId() : 0 );

		if ( $namespace === null ) {
			if ( $uid != 0 ) {
				$out = $user->getEditCount();
			} else {
				$out = '';
			}
		} else {
			$ns = $services->getNamespaceInfo()->getCanonicalIndex( strtolower( $namespace ) );
			$out = Editcount::editsInNs( $user, $ns );
		}

		return $out;
	}
}
