<?php

use MediaWiki\MediaWikiServices;
use MediaWiki\User\UserFactory;

class Editcount extends IncludableSpecialPage {

	/** @var UserFactory */
	private $userFactory;

	/**
	 * Constructor
	 */
	public function __construct( UserFactory $userFactory ) {
		parent::__construct( 'Editcount' );

		$this->userFactory = $userFactory;
	}

	/**
	 * Show the special page
	 *
	 * @param string|null $subPage User name, optionally followed by a namespace, or null
	 */
	public function execute( $subPage ) {
		$target = $subPage ?: $this->getRequest()->getText( 'username' );

		list( $username, $namespace ) = $this->extractParameters( $target );
		$this->getOutput()->enableOOUI();

		$user = $this->userFactory->newFromName( $username );
		$username = is_object( $user ) ? $user->getName() : '';

		$uid = ( $user instanceof User ? $user->getId() : 0 );

		if ( $this->including() ) {
			$contLang = MediaWikiServices::getInstance()->getContentLanguage();
			if ( $namespace === null ) {
				if ( $uid != 0 ) {
					$out = $contLang->formatNum( $user->getEditCount() );
				} else {
					$out = '';
				}
			} else {
				$out = $contLang->formatNum( $this->editsInNs( $user, $namespace ) );
			}
			$this->getOutput()->addHTML( $out );
		} else {
			$nscount = [];
			$total = 0;
			if ( $uid ) {
				$nscount = $this->editsByNs( $user );
				$total = $this->getTotal( $nscount );
			}
			$html = new EditcountHTML( $this->userFactory );
			$html->setContext( $this->getContext() );

			$html->outputHTML( $username, $uid, $nscount, $total );
		}
	}

	/**
	 * Parse the username and namespace parts of the input and return them
	 *
	 * @param string $par
	 *
	 * @return array
	 */
	private function extractParameters( string $par ) : array {
		// @fixme don't use @
		@list( $user, $namespace ) = explode( '/', $par, 2 );

		// str*cmp sucks
		if ( isset( $namespace ) ) {
			$namespace = MediaWikiServices::getInstance()->getContentLanguage()->getNsIndex( $namespace );
		}

		return [ $user, $namespace ];
	}

	/**
	 * Compute and return the total edits in all namespaces
	 *
	 * @param array $nscount An associative array
	 *
	 * @return int
	 */
	private function getTotal( array $nscount ) : int {
		$total = 0;
		foreach ( array_values( $nscount ) as $i ) {
			$total += $i;
		}

		return $total;
	}

	/**
	 * Count the number of edits of a user by namespace
	 *
	 * @param User|null $user The user to check
	 *
	 * @return array
	 */
	static function editsByNs( ?User $user ) : array {
		if ( $user === null ) {
			return [];
		}

		$dbr = wfGetDB( DB_REPLICA );
		$actorWhere = ActorMigration::newMigration()->getWhere( $dbr, 'rev_user', $user );
		$res = $dbr->select(
			[ 'revision', 'page' ] + $actorWhere['tables'],
			'page_namespace, COUNT(*) as count',
			[ $actorWhere['conds'] ],
			__METHOD__,
			[ 'GROUP BY' => 'page_namespace' ],
			[ 'page' => [ 'JOIN', 'page.page_id = rev_page' ] ] + $actorWhere['joins']
		);

		$nscount = [];
		foreach ( $res as $row ) {
			$nscount[$row->page_namespace] = $row->count;
		}

		return $nscount;
	}

	/**
	 * Count the number of edits of a user in a given namespace
	 *
	 * @param User|null $user The user to check
	 * @param int $ns The namespace to check
	 *
	 * @return int
	 */
	static function editsInNs( ?User $user, int $ns ) : int {
		if ( $user === null ) {
			return 0;
		}

		$dbr = wfGetDB( DB_REPLICA );
		$actorWhere = ActorMigration::newMigration()->getWhere( $dbr, 'rev_user', $user );
		return (int)$dbr->selectField(
			[ 'revision', 'page' ] + $actorWhere['tables'],
			'COUNT(*)',
			[ 'page_namespace' => $ns, $actorWhere['conds'] ],
			__METHOD__,
			[],
			[ 'page' => [ 'JOIN', 'page.page_id = rev_page' ] ] + $actorWhere['joins']
		);
	}
}

class EditcountHTML extends Editcount {
	/**
	 * @var array
	 */
	private $nscount;

	/**
	 * @var int
	 */
	private $total;

	/**
	 * Output the HTML form on Special:Editcount
	 *
	 * @param string $username
	 * @param int $uid
	 * @param array $nscount
	 * @param int $total
	 */
	function outputHTML( string $username, int $uid, array $nscount, int $total ) {
		$this->nscount = $nscount;
		$this->total = $total;

		$this->setHeaders();

		$action = htmlspecialchars( $this->getPageTitle()->getLocalURL() );

		$input = new OOUI\ActionFieldLayout(
			new OOUI\TextInputWidget( [
				'name' => 'username',
				'value' => $username,
				'autofocus' => true,
			] ),
			new OOUI\ButtonInputWidget( [
				'label' => $this->msg( 'editcount_submit' )->text(),
				'flags' => [ 'primary', 'progressive' ],
				'type' => 'submit',
			] ),
			[
				'align' => 'top',
				'label' => $this->msg( 'editcount_username' )->escaped()
			]
		);

		$out = "
		<form id='editcount' method='post' action=\"$action\">
			<table>
				<tr>
					<td>" . $input . "</td>
				</tr>";
		if ( $username != null && $uid != 0 ) {
			$editcounttable = $this->makeTable();
			$out .= "
				<tr>
					<td>$editcounttable</td>
				</tr>";
		}
		$out .= '
			</table>
		</form>';
		$this->getOutput()->addHTML( $out );
	}

	/**
	 * Make the editcount-by-namespaces HTML table
	 *
	 * @return string
	 */
	private function makeTable() : string {
		$lang = $this->getLanguage();

		$contentNamespaces = MediaWikiServices::getInstance()->getNamespaceInfo()->getContentNamespaces();

		$total = $this->msg( 'editcount_total' )->escaped();
		$ftotal = $lang->formatNum( $this->total );
		$percent = $this->total > 0 ? wfPercent( 100 ) : wfPercent( 0 ); // @bug 4400
		$ret = "<table class='wikitable'>
				<tr>
					<th>$total</th>
					<th>$ftotal</th>
					<th>$percent</th>
				</tr>
		";

		$ctotal = 0;

		foreach ( $this->nscount as $ns => $edits ) {
			$fedits = $lang->formatNum( $edits );
			$fns = ( $ns == NS_MAIN ) ? $this->msg( 'blanknamespace' ) : $lang->getFormattedNsText( $ns );
			$fpercent = $this->formatPercent( $edits / $this->total * 100 );
			$ret .= "
				<tr>
					<td>$fns</td>
					<td>$fedits</td>
					<td>$fpercent</td>
				</tr>
			";

			if ( in_array( $ns, $contentNamespaces ) ) {
				$ctotal += $edits;
			}
		}

		$fcontentns = $this->msg( 'editcount_content_ns' );
		$fctotal = $lang->formatNum( $ctotal );
		$fcpercent = $this->total > 0 ? $this->formatPercent( $ctotal / $this->total * 100 ) : wfPercent( 0 );

		$ret .= "
				<tr style='font-weight: bold'>
					<td>$fcontentns</td>
					<td>$fctotal</td>
					<td>$fcpercent</td>
				</tr>
			";

		$ret .= '</table>
		';

		return $ret;
	}

	/**
	 * Because built-in MW functions are a mess.
	 *
	 * @param int|float|null $x The numeric percentage.
	 *
	 * @return string
	 */
	private function formatPercent( $x ) : string {
		$r = sprintf( "%.2f", $x ?: 0 );
		return $this->getLanguage()->formatNum( round( (float)$r, 2 ) ) . '%';
	}
}
